# Introduction
This is a rails application that serves as a standardized RESTful API for scheduling software Amion and Webexchange.

# Getting Started

## Customizing the code 
1. Set the username and password
Go to or create /config/application.yml and fill in the following
EXTRANET_PASSWORD: "USERNAME"
EXTRANET_USERNAME: "PASSWORD"
EXCEPTION_RECIPIENTS: "a@a.com,b@b.com"

2. Edit the mailer settings in development.rb and production.rb

3. Edit update_mailer.rb

## Deployment and Setup
1. Seed the database with services

```ruby
rake webexchange:seed
```

2. Fetch services

```ruby
rake webexchange:fetch_all
```

3. See the list of services
http://localhost:3000

4. Get the json output for a service
http://localhost:3000/services/infectious-diseases-ppmc.json

5. Get the raw html for a service
http://localhost:3000/services/infectious-diseases-ppmc

## Setting up users
1. Set up a user at the following
http://localhost:3000/users/create
2. Be aware of the following
After the first user has been created, all GET requests involving data will require a user to be signed in. To do this, include the following parameter with your GET request:
?key=KEY
Admins are a type of user
After the first admin has been created, all data creation tasks require an admin to be signed in, and all User admin tasks require an admin

## Setting up automatic updating

set up a cron job to run the following:
```ruby
rake webexchange:fetch_all
rake amion:fetch_all
```

## Set up a pinger