class Webexchange < ActiveRecord::Base
  
  # require_relative '../../lib/WebexchangeAdapter.rb'
  # require_relative '../../lib/WebexchangeParser.rb'
  # 
  # def self.match_service(code)
  #   if all_services.collect{|k, v| k}.include? code
  #     code
  #   else
  #     res = all_services.detect { |k, v| code == k.strip }#returns [code, name] or nil
  #     res.nil? ? nil : res.first  
  #   end
  # end
  # 
  # def self.all_services
  #   services = {
  #         "    ADMHUP" => "Administration O/C, HUP                 ",
  #         "      AD91" => "Administration, PPMC                    ",
  #         "      PC07" => "Adolescent &amp; Young Adult Associates     ",
  #         "   PENCARD" => "Adult Congenital Heart Center           ",
  #         "    ADVLUN" => "Advanced Lung Disease O/C               ",
  #         "      LC25" => "Allergy Clinic                          ", #
  #         "      AN44" => "Anesthesia O/C PPMC                     ",
  #         "      L989" => "Anesthesia, HUP                         ",
  #         "      FA63" => "Ankle &amp; Foot O/C, PPMC                  ",
  #         "     AONCL" => "Attorney On Call, GSPP                  ",
  #         "      BBST" => "Buckley, Braffman, Stern                ",
  #         "      CT95" => "Cardiovascular Surgery, 3 Sites         ",
  #         "      6770" => "Cath Lab, HUP                           ",
  #         "      CHF0" => "Chestnut Hill Family Medicine           ",
  #         "      F240" => "Clinical Nutrition Support              ",
  #         "      O303" => "Coagulation Lab                         ",
  #         "      MARK" => "Contact Center                          ",
  #         "      3238" => "Cytopathology                           ",
  #         "      LANC" => "Delancey Medical Associates             ",
  #         "    ABRCAC" => "Donors &amp; Benefactors                    ",
  #         "    DOYRAD" => "Doylestown Radiation Oncology           ",
  #         "      EAST" => "East Marshall Street                    ",
  #         "      G375" => "Echocardiology                          ",
  #         "      ESCP" => "Edward S.Cooper Practice                ",
  #         "      EPMC" => "Endocrinology, Dr. Finkel               ",
  #         "    ENTONC" => "ENT, PPMC                               ",
  #         "      EPS1" => "EPS Office, HUP                         ",
  #         "    ETHICS" => "Ethics Committee, HUP                   ",
  #         "      J280" => "Facial Trauma O/C                       ",
  #         "  PERELGAS" => "Gastroenterology, HUP                   ",
  #         "      GE00" => "Gastroenterology, PPMC                  ",
  #         "      2984" => "Geriatric Medicine                      ",
  #         "      3318" => "Gyn Oncology                            ",
  #         "   GYNPPMC" => "Gynecology @ PPMC                       ",
  #         "      P231" => "Gynecology, Dickens Cntr                ",
  #         "      6106" => "Hand Center, PPMC                       ",
  #         "      HSHP" => "Hand Surgery ER O/C                     ",
  #         "      N319" => "Heart Failure Clinic                    ",
  #         "      HOON" => "Hematology Onco, PPMC                   ",
  #         "      G203" => "Hematology Oncology, HUP                ",
  #         "    HOSPPM" => "Hospitalist O/C, PPMC                   ",
  #         "      2364" => "Hyperbaric Medicine                     ",
  #         "   PPMCINF" => "Infection Control &amp; Prevention, PMC     ",
  #         "      B143" => "Infection Control, HUP                  ",
  #         "      E296" => "Infectious Disease, HUP                 ",
  #         "      ID22" => "Infectious Diseases, PPMC               ",
  #         "      1146" => "Infertility, HUP                        ",
  #         "      PEMA" => "Internal Medicine at Mayfair            ",
  #         "  INTRADPR" => "Interventional Radiology HUP&amp;PPMC       ",
  #         "      CCAJ" => "J. Edwin Wood Clinic                    ",
  #         "      5232" => "Legal Affairs                           ",
  #         "      LFPN" => "Life Program                            ",
  #         "    MRIHUP" => "MRI O/C, HUP                            ",
  #         "  NEEDLPRE" => "Needle Sticks O/C, PPMC                 ",
  #         "    NEDSTX" => "Needle Sticks, HUP                      ",
  #         "    NEUINT" => "Neuro Interventional Radiology O/C      ",
  #         "      2700" => "Neurology, HUP                          ", #
  #         "      NE70" => "Neurology, PPMC                         ",
  #         "    NEURAD" => "Neuroradiology O/C                      ",
  #         "   NEUHUPP" => "Neurosurgery O/C, HUP&amp;PPMC              ",
  #         "    NUCMED" => "Nuclear Medicine O/C, HUP               ",
  #         "    OBGYNH" => "OB/GYN O/C, HUP                         ",
  #         "      MS21" => "Oral &amp; Maxillofacial, PPMC              ",
  #         "    DENTAL" => "Oral Maxillofacial Surg, HUP            ",
  #         "      M568" => "Orthopaedics, HUP                       ",
  #         "      2777" => "OTO, HUP                                ",
  #         "      PAOR" => "PAH Foot &amp; Ankle Surgeons               ",
  #         "    PAHGIS" => "PAH G.I. Surgery                        ",
  #         "    PAHPLB" => "PAH Plastic Surgery, Dr. Bucky          ",
  #         "      MFMP" => "PAH, Maternal Fetal Med                 ",
  #         "      PA63" => "Pathology, PPMC                         ",
  #         "   CORPPAT" => "Patient Facilitated Services            ",
  #         "      PP90" => "Penn Center for Primary Care            ",
  #         "  PECAVAFO" => "Penn Family Med Valley Forge            ",
  #         "    PENBAL" => "Penn Medicine Bala Cynwyd Med Assoc.    ",
  #         "   RITHSTU" => "Penn Medicine Rittenhouse               ",
  #         "    PENOBY" => "Penn Ob Gyn, POGA                       ",
  #         "      4090" => "Penn Ortho O/C, PPMC                    ",
  #         "      MSM1" => "Penn Presbyterian Med Assocs.           ",
  #         "      C690" => "Penn Transplant Center                  ",
  #         "  PENBUCCO" => "PENNCare Bucks Co.                      ",
  #         "    KENSQR" => "PENNCare Kennett Family Practice        ",
  #         "      PEKI" => "PENNCare Kids, Phoenixville             ",
  #         "  PENCFRPX" => "PENNCare Main Street Family Care        ",
  #         "  H6450056" => "PennSTAR Medical Command MD Schedule    ",
  #         "      A294" => "Physical Medicine &amp; Rehabilitation      ",
  #         "      PIMS" => "PIMA, Penn Internal Medicine Assoc.     ",
  #         "      7090" => "Plastic Surgery Clinic                  ",
  #         "    ENDPMC" => "PPMC Endocrinology O/C                  ",
  #         "  PENFAMOB" => "Pregnancy Loss &amp; Penn Family Planning   ",
  #         "    PRICAR" => "Private Cardiology O/C, HUP             ",
  #         "    PSYCAL" => "Psychiatry O/C, HUP                     ",
  #         "      PULM" => "Pulmonary, HUP                          ", #
  #         "      J202" => "Pulmonary, PPMC                         ",
  #         "  RADPEREL" => "Radiation Oncology, HUP                 ",
  #         "    RAONVA" => "Radiation Oncology, Valley Forge        ",
  #         "      RA99" => "Radiology O/C, PPMC                     ",
  #         "      INME" => "Radnor, Internal Med Grp                ",
  #         "      WMOG" => "Radnor, Penn Health for Women           ",
  #         "      2690" => "Renal, HUP                              ",
  #         "      RC29" => "Renal, PPMC                             ",
  #         "      2454" => "Rheumatology, HUP &amp; PPMC                ",
  #         "      RMAS" => "Royersford Medical Associates           ",
  #         "      N428" => "Scheie Eye O/C, PPMC                    ",
  #         "      SLEP" => "Sleep Medicine O/C                      ",
  #         "      L132" => "Social Work                             ",
  #         "      CR20" => "Social Work O/C, PPMC                   ",
  #         "    SPIONC" => "Spine Fellow O/C, HUP                   ",
  #         "      PESP" => "Spruce Internal Med. Assoc.             ",
  #         "    CAPSTU" => "Student Health Psych-CAPS               ",
  #         "      2850" => "Student Health Services                 ",
  #         "      2490" => "Telecommunications                      ",
  #         "    PMCTHO" => "Thoracic Surgery, PPMC                  ",
  #         "    TISTYP" => "Tissue Typing Tech                      ",
  #         "      CL83" => "Transfusion Service                     ",
  #         "      2949" => "Transplant Nurse O/C                    ",
  #         "      UCF0" => "University City Family Medicine         ",
  #         "      B140" => "Urology Clinic, HUP                     ",
  #         "   UROSURG" => "Urology Surgery, PPMC                   ",
  #         "   VAVDEN2" => "VAD-LVAD, PPMC                          ",
  #         "      RA87" => "Vascular Lab, PPMC                      ",
  #         "      WCF0" => "West Chester Family Practice            ",
  #         "      WH77" => "Wissahickon Hospice                     ",
  #         "    CARDHU" => "Cardiology, HUP                         ", 
  #         "    CARPMC" => "Cardiology, PPMC                        ", 
  #         "    HUPVAS" => "HUP Vascular O/C                        ",
  #         "    HUPCAD" => "HUP Cardiac O/C                         ",
  #         "    HUPTRA" => "HUP Trauma All O/C                      ",
  #         "    HUPTHO" => 'HUP Thoracic O/C                        '
  # 
  #   }
  #   
  #    services_to_fetch = ['HUPTHO', 'HUPTRA', 'HUPCAD', 'HUPVAS', 'NEURAD','HSHP', 'CARPMC', 'CARDHU', "LC25","2690", "G203", "PULM", "2454", "PERELGAS", "B140", "GE00", "HOON", "RC29", "J202", "UROSURG", "ID22", "NE70", "RC29", "7090", "SLEP", "GYNPPMC", "EPS1"] # 
  #     discarded_services = services.select {|k,v| !services_to_fetch.include?(k.strip)}
  #     services.keep_if {|k,v| services_to_fetch.include? k.strip}
  #   
  # end
  # 
  # def this_month_as_objects
  #   require_relative '../../lib/WebexchangeAdapter.rb'
  #   require_relative '../../lib/WebexchangeParser.rb'
  #   WebexchangeParser.new(this_month).as_json
  # end
  # 
  # def next_month_as_objects
  #   require_relative '../../lib/WebexchangeAdapter.rb'
  #   require_relative '../../lib/WebexchangeParser.rb'
  #   WebexchangeParser.new(next_month).as_json
  # end
  # 
  # def self.check
  #   services = Webexchange.all_services
  #   alert = false
  #   message = []
  #   services.each do |code, name|
  #     most_recent = Webexchange.where(service_code: code).order(:created_at).last
  #     last_updated = most_recent.created_at
  #     if (DateTime.now > (last_updated + 1.days))
  #       alert = true
  #       message.push "Error adding service #{name} - last updated on #{last_updated}"
  #     end
  #     if most_recent.this_month_as_objects.nil? or most_recent.this_month_as_objects.empty? or most_recent.this_month_as_objects == []
  #       alert = true
  #       message.push "Error adding service #{name} - last entry was empty, updated on #{last_updated}"
  #     end
  #   end
  #   
  #   if alert
  #     puts message
  #     UpdateMailer.send_message(message).deliver
  #   else
  #     puts 'passed all tests'
  #     if Time.now.hour < 1
  #       UpdateMailer.send_message('passed all tests').deliver
  #     end
  #   end
  #   
  # end
  # 
  # def self.update
  #   require_relative '../../lib/WebexchangeAdapter.rb'
  #   require_relative '../../lib/WebexchangeParser.rb'
  #   services = Webexchange.all_services
  #   
  #   f = WebexchangeAdapter.new
  #   # services = ['    HUPVAS']
  #   services.each do |code,name|
  #     data = []
  #     
  #     this_month = f.get_raw_source(code, Date.today.beginning_of_month)
  #     next_month = f.get_raw_source(code, Date.today.next_month.beginning_of_month)
  #     Webexchange.create(service_code: code, name: name, this_month: this_month, next_month: next_month)
  #   end
  # end
  
  
end
