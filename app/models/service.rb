# this is the parent class of webexchange_services, coast_services, and amion_services etc

class Service < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  @date_array = nil
  
  # download remote page and store the raw html
  def fetch
  end
  
  # parse pre-fetched raw html into a dict or hash
  def parse
  end
  
  def date_range
    if date_start and date_end
      "#{date_start.strftime("%D") if date_start }-#{date_end.strftime("%D") if date_end}"
    else
      nil
    end
  end
  
  def date_start
    h = date_array.first
    if h and h[:shift_start]
      Time.parse(h[:shift_start])
    else
      nil
    end
  end
  
  def date_end
    h = date_array.last
    if h and h[:shift_start]
      Time.parse(h[:shift_start])
    else
      nil
    end
  end
  
  def date_array
    @date_array ||= (parse || [])
  end
end
