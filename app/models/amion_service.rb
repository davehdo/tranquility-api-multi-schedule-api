class AmionService < Service
  require 'AmionAdapter.rb'
  
  def fetch( adapter=nil )
    adapter ||= AmionAdapter.new( self.remote_id )
    self.raw_html_1 = adapter.fetch_shifts(Date.today)
    self.raw_html_2 = adapter.fetch_shifts(Date.today.next_month)
    touch
    save
  end
  
  def parse
    AmionAdapter.parse_all_shifts( self.raw_html_1 || "") +
      AmionAdapter.parse_all_shifts( self.raw_html_2 || "")
  end
end