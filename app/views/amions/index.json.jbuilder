json.array!(@amions) do |amion|
  json.extract! amion, :id, :account, :users, :rows, :on_call, :all_shifts
  json.url amion_url(amion, format: :json)
end
