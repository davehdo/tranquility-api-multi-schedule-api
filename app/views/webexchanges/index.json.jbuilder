json.array!(@webexchanges) do |webexchange|
  json.extract! webexchange, :id, :name, :service_code, :this_month, :next_month
  json.url webexchange_url(webexchange, format: :json)
end
