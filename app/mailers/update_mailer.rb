class UpdateMailer < ActionMailer::Base
  
  def send_message(log)
    @text = log
    mail(to: 'eugyev@gmail.com', subject: "Rolodoc Update", from: "eugyev@gmail.com")
  end
  
end
