namespace :webexchange do
  require 'WebexchangeAdapter.rb'
  
  task :seed => :environment do
    services = {
    			"    ADMHUP" => "Administration O/C, HUP                 ",
    			"      AD91" => "Administration, PPMC                    ",
    			"      PC07" => "Adolescent &amp; Young Adult Associates     ",
    			"   PENCARD" => "Adult Congenital Heart Center           ",
    			"    ADVLUN" => "Advanced Lung Disease O/C               ",
    			"      LC25" => "Allergy Clinic                          ", #
    			"      AN44" => "Anesthesia O/C PPMC                     ",
    			"      L989" => "Anesthesia, HUP                         ",
    			"      FA63" => "Ankle &amp; Foot O/C, PPMC                  ",
    			"     AONCL" => "Attorney On Call, GSPP                  ",
    			"      BBST" => "Buckley, Braffman, Stern                ",
    			"      CT95" => "Cardiovascular Surgery, 3 Sites         ",
    			"      6770" => "Cath Lab, HUP                           ",
    			"      CHF0" => "Chestnut Hill Family Medicine           ",
    			"      F240" => "Clinical Nutrition Support              ",
    			"      O303" => "Coagulation Lab                         ",
    			"      MARK" => "Contact Center                          ",
    			"      3238" => "Cytopathology                           ",
    			"      LANC" => "Delancey Medical Associates             ",
    			"    ABRCAC" => "Donors &amp; Benefactors                    ",
    			"    DOYRAD" => "Doylestown Radiation Oncology           ",
    			"      EAST" => "East Marshall Street                    ",
    			"      G375" => "Echocardiology                          ",
    			"      ESCP" => "Edward S.Cooper Practice                ",
    			"      EPMC" => "Endocrinology, Dr. Finkel               ",
    			"    ENTONC" => "ENT, PPMC                               ",
    			"      EPS1" => "EPS Office, HUP                         ",
    			"    ETHICS" => "Ethics Committee, HUP                   ",
    			"      J280" => "Facial Trauma O/C                       ",
    			"  PERELGAS" => "Gastroenterology, HUP                   ",
    			"      GE00" => "Gastroenterology, PPMC                  ",
    			"      2984" => "Geriatric Medicine                      ",
    			"      3318" => "Gyn Oncology                            ",
    			"   GYNPPMC" => "Gynecology @ PPMC                       ",
    			"      P231" => "Gynecology, Dickens Cntr                ",
    			"      6106" => "Hand Center, PPMC                       ",
    			"      HSHP" => "Hand Surgery ER O/C                     ",
    			"      N319" => "Heart Failure Clinic                    ",
    			"      HOON" => "Hematology Onco, PPMC                   ",
    			"      G203" => "Hematology Oncology, HUP                ",
    			"    HOSPPM" => "Hospitalist O/C, PPMC                   ",
    			"      2364" => "Hyperbaric Medicine                     ",
    			"   PPMCINF" => "Infection Control &amp; Prevention, PMC     ",
    			"      B143" => "Infection Control, HUP                  ",
    			"      E296" => "Infectious Disease, HUP                 ",
    			"      ID22" => "Infectious Diseases, PPMC               ",
    			"      1146" => "Infertility, HUP                        ",
    			"      PEMA" => "Internal Medicine at Mayfair            ",
    			"  INTRADPR" => "Interventional Radiology HUP&amp;PPMC       ",
    			"      CCAJ" => "J. Edwin Wood Clinic                    ",
    			"      5232" => "Legal Affairs                           ",
    			"      LFPN" => "Life Program                            ",
    			"    MRIHUP" => "MRI O/C, HUP                            ",
    			"  NEEDLPRE" => "Needle Sticks O/C, PPMC                 ",
    			"    NEDSTX" => "Needle Sticks, HUP                      ",
    			"    NEUINT" => "Neuro Interventional Radiology O/C      ",
    			"      2700" => "Neurology, HUP                          ", #
    			"      NE70" => "Neurology, PPMC                         ",
    			"    NEURAD" => "Neuroradiology O/C                      ",
    			"   NEUHUPP" => "Neurosurgery O/C, HUP&amp;PPMC              ",
    			"    NUCMED" => "Nuclear Medicine O/C, HUP               ",
    			"    OBGYNH" => "OB/GYN O/C, HUP                         ",
    			"      MS21" => "Oral &amp; Maxillofacial, PPMC              ",
    			"    DENTAL" => "Oral Maxillofacial Surg, HUP            ",
    			"      M568" => "Orthopaedics, HUP                       ",
    			"      2777" => "OTO, HUP                                ",
    			"      PAOR" => "PAH Foot &amp; Ankle Surgeons               ",
    			"    PAHGIS" => "PAH G.I. Surgery                        ",
    			"    PAHPLB" => "PAH Plastic Surgery, Dr. Bucky          ",
    			"      MFMP" => "PAH, Maternal Fetal Med                 ",
    			"      PA63" => "Pathology, PPMC                         ",
    			"   CORPPAT" => "Patient Facilitated Services            ",
    			"      PP90" => "Penn Center for Primary Care            ",
    			"  PECAVAFO" => "Penn Family Med Valley Forge            ",
    			"    PENBAL" => "Penn Medicine Bala Cynwyd Med Assoc.    ",
    			"   RITHSTU" => "Penn Medicine Rittenhouse               ",
    			"    PENOBY" => "Penn Ob Gyn, POGA                       ",
    			"      4090" => "Penn Ortho O/C, PPMC                    ",
    			"      MSM1" => "Penn Presbyterian Med Assocs.           ",
    			"      C690" => "Penn Transplant Center                  ",
    			"  PENBUCCO" => "PENNCare Bucks Co.                      ",
    			"    KENSQR" => "PENNCare Kennett Family Practice        ",
    			"      PEKI" => "PENNCare Kids, Phoenixville             ",
    			"  PENCFRPX" => "PENNCare Main Street Family Care        ",
    			"  H6450056" => "PennSTAR Medical Command MD Schedule    ",
    			"      A294" => "Physical Medicine &amp; Rehabilitation      ",
    			"      PIMS" => "PIMA, Penn Internal Medicine Assoc.     ",
    			"      7090" => "Plastic Surgery Clinic                  ",
    			"    ENDPMC" => "PPMC Endocrinology O/C                  ",
    			"  PENFAMOB" => "Pregnancy Loss &amp; Penn Family Planning   ",
    			"    PRICAR" => "Private Cardiology O/C, HUP             ",
    			"    PSYCAL" => "Psychiatry O/C, HUP                     ",
    			"      PULM" => "Pulmonary, HUP                          ", #
    			"      J202" => "Pulmonary, PPMC                         ",
    			"  RADPEREL" => "Radiation Oncology, HUP                 ",
    			"    RAONVA" => "Radiation Oncology, Valley Forge        ",
    			"      RA99" => "Radiology O/C, PPMC                     ",
    			"      INME" => "Radnor, Internal Med Grp                ",
    			"      WMOG" => "Radnor, Penn Health for Women           ",
    			"      2690" => "Renal, HUP                              ",
    			"      RC29" => "Renal, PPMC                             ",
    			"      2454" => "Rheumatology, HUP &amp; PPMC                ",
    			"      RMAS" => "Royersford Medical Associates           ",
    			"      N428" => "Scheie Eye O/C, PPMC                    ",
    			"      SLEP" => "Sleep Medicine O/C                      ",
    			"      L132" => "Social Work                             ",
    			"      CR20" => "Social Work O/C, PPMC                   ",
    			"    SPIONC" => "Spine Fellow O/C, HUP                   ",
    			"      PESP" => "Spruce Internal Med. Assoc.             ",
    			"    CAPSTU" => "Student Health Psych-CAPS               ",
    			"      2850" => "Student Health Services                 ",
    			"      2490" => "Telecommunications                      ",
    			"    PMCTHO" => "Thoracic Surgery, PPMC                  ",
    			"    TISTYP" => "Tissue Typing Tech                      ",
    			"      CL83" => "Transfusion Service                     ",
    			"      2949" => "Transplant Nurse O/C                    ",
    			"      UCF0" => "University City Family Medicine         ",
    			"      B140" => "Urology Clinic, HUP                     ",
    			"   UROSURG" => "Urology Surgery, PPMC                   ",
    			"   VAVDEN2" => "VAD-LVAD, PPMC                          ",
    			"      RA87" => "Vascular Lab, PPMC                      ",
    			"      WCF0" => "West Chester Family Practice            ",
    			"      WH77" => "Wissahickon Hospice                     ",
    			"    CARDHU" => "Cardiology, HUP                         ", 
    			"    CARPMC" => "Cardiology, PPMC                        ", 
    			"    HUPVAS" => "HUP Vascular O/C                        ",
    			"    HUPCAD" => "HUP Cardiac O/C                         ",
    			"    HUPTRA" => "HUP Trauma All O/C                      ",
    			"    HUPTHO" => 'HUP Thoracic O/C                        '

    }
    
    services_to_fetch = ['HUPTHO', 'HUPTRA', 'HUPCAD', 'HUPVAS', 'NEURAD','HSHP', 'CARPMC', 'CARDHU', "LC25","2690", "G203", "PULM", "2454", "PERELGAS", "B140", "GE00", "HOON", "RC29", "J202", "UROSURG", "ID22", "NE70", "RC29", "7090", "SLEP", "GYNPPMC", "EPS1"] # 
    services.keep_if {|k,v| services_to_fetch.include? k.strip}.each do |service_code, service_name|
      WebexchangeService.create name: service_name.strip, remote_id: service_code
    end
  end
  
  desc "just run rake webexchange:update to get webexchange schedules"
  task :fetch_all => :environment do
    f = WebexchangeAdapter.new( ENV["EXTRANET_USERNAME"], ENV["EXTRANET_PASSWORD"])
    services = WebexchangeService.order("updated_at ASC")
    puts "==Fetching #{services.size} services, starting with the oldest ones"
    services.each do |service|
      puts "=Now fetching #{service.name}"
      service.fetch(f) # pass in the adapter to prevent the need for multiple signons
    end
    puts "success"
  end
  
  # desc "just run rake webexchange:check to check for errors"
  # task :check => :environment do
  #   Webexchange.check
  #   puts "success"
  # end
  
end