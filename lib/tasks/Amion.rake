namespace :amion do
  task :seed => :environment do
      [
        'vaphil inpat', 
        'vaphil behav',
        'vaphil medic',
        'vaphil surge',
        'pennres imr',
        'pennres cardres'
      ].each do |account|
        AmionService.create name: account.strip, remote_id: account
      end
  end
  
  desc "just run rake amion:update to get amion schedules"
  task :fetch_all => :environment do

    services = AmionService.order("updated_at ASC")
    puts "==Fetching #{services.size} services, starting with the oldest ones"
    services.each do |service|
      puts "=Now fetching #{service.name}"
      service.fetch # no need to pass in the adapter because there is no authentication
          # instead, just need to pass the account name with each GET request
    end
    puts "success"
  end
  
end