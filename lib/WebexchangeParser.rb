# the timezone offet is hardcoded below: I would like to change this ultimately

class WebexchangeParser
  	attr_accessor :service, :doc_title, :role_names, :as_json
    
  	def initialize( raw_html = nil )
      @timezone_offset = "-04:00"
      if raw_html and raw_html.strip != ""
        # self.doc_title = raw_html.scan(/<span class=subheadline>Monthly On-Call Schedule for: (.*?)<\/span>/)[0][0]
      end
      self.add raw_html
  	end

  	def add( raw_html )
  	  self.get_role_names( raw_html )
  		self.parse( raw_html )		
    end

  	# returns an array of raw html for each section that says "As of:" 
  	def html_by_timeslot( raw_html )
  	  if raw_html
    		cells = raw_html.scan(/<td tabindex="\d*".*?<\/table>\W*?<\/td>/m)

    		cells.collect do |cell|			
    			timeslots = cell.scan(/As of:<br\/>(.*?)<\/u>(.*?)<\/table>/m)
    		end.flatten(1)
  		else
  		  []
  		end
  	end

  	# e.g. ["Attending", "Fellow"] this is only present in left-most column 
  	# order matters here, because we use this to match with people's names/numbers
  	def get_role_names( raw_html )
  	  if raw_html
    		self.role_names = raw_html.scan(/<td [^\<\>]*?nowrap><b>(.*?)<\/b><\/td>/).collect{|e| e[0]}
        # self.role_names = self.role_names.uniq
  		end
  		self.role_names
  	end

    def timezone_offset
      @timezone_offset
    end
    
    def transform_date( d ) # input format: "09/01/2013 12:00 PM"
  		Time.new( d.slice(6,4).to_i, d.slice(0,2).to_i, d.slice(3,2).to_i, self.hour_corrected(d) , d.slice(14,2 ).to_i, 0, timezone_offset )  
  	end

  	def hour_corrected( d ) # converts 0-12 hour to 0-23 given AM/PM
  		hr = d.slice(11,2).to_i 
  		pm = d.slice(17,2) == "PM"
  		if hr == 12
  			hr + ( pm ? 0 : -12)
  		else
  			hr + ( pm ? 12 : 0)
  		end
  	end

  	# stores as as_json
  	def parse( raw_html )
      # if self.role_names.nil?
      #   puts "can't find role names" 
      # else
        self.as_json ||= []
  			self.as_json += self.html_by_timeslot( raw_html ).collect do |timeslot|				

  				# e.g. 07/28/2013 07:00 AM 
  				datetime = transform_date( timeslot[0] )

  					a = self.role_names.clone # e.g. ["Attending", "Fellow"] 
            # puts "--#{a}--"
            # puts "++#{timeslot[1].scan(/<td nowrap>([^\<\>]*?)<\/td>/m)}++"
            # raise "hi"
  					names_numbers = timeslot[1].scan(/<td nowrap>([^\<\>]*?)<\/td>/m).collect do |nn|
  						if (c = nn[0].scan(/^(.+?), (\d+)$/)[0]) != nil # match the [name], [number] format, leave blank
  							{ shift_start: datetime.iso8601, consultant: c[0].gsub(/\u0026nbsp;/, " ").strip, number: c[1], role: a.shift }
  						elsif (c = nn[0].scan(/^(.+?)$/)[0]) != nil
  							{ shift_start: datetime.iso8601, consultant: c[0].gsub(/\u0026nbsp;/, " ").strip, role: a.shift }
  						else
  							a.shift # waste a role_name so the next one is correct
  							nil
  						end
  					end # do
  			end.flatten.compact
      # end # if role_names.nil?

  	end

  	def save( fname = nil  )\
  		fname ||= self.doc_title || "just-saved"

  		# save the raw html file in the /coast directory on your local machine
      directory_name = "files/webexchange"
      Dir.mkdir(directory_name) unless File.exists?(directory_name)

  		 f = File.open( "#{directory_name}/#{fname}" ,'w')
  		 f.write(self.as_json.to_json)
  		 f.close
  		 puts "Saved as #{ fname }" 
  		true
  	end

end