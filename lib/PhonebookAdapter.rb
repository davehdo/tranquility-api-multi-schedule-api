### how to use
### PhonebookAdapter.new(extranet_username, extranet_pw).search(lname, fname)
## results in the form of [{
#  :name=>"DO, DAVID", 
#  :location=>"HUP  Neurology  Department  of  3  W  Gates ", 
#  "Office"=>"(215) 662-2700"
#  "Fax"=>"(215) 349-5579"
#   "Cell"=>"(215) 520-5868", 
#   "email"=>"david.do@uphs.upenn.edu"}, 
#   :text_page_address=>["2155205868@page.att.net"]}

class PhonebookAdapter
  require 'PhonebookParser.rb'
  def initialize(username, password)
    @penn_extranet_adapter = PennExtranetAdapter.new( username, password)
  end
  
  def authenticated_agent
    @penn_extranet_adapter.authenticated_agent
  end
  
  def search (lname = '', fname = '')
    uri = create_uri
    params = post_params
    params[first_name_key] = fname
    params[last_name_key] = lname
    params[phone_number_key]
    agent = onVPN? ? Mechanize.new : @penn_extranet_adapter.authenticated_agent
    agent.post(uri, params)
    PhonebookParser.new.parse(agent.page)
  end
  
  def create_uri
    url = onVPN? ? 'http://uphsxnet.uphs.upenn.edu/pb/main/Results.aspx' : "https://extranet.uphs.upenn.edu/pb/main/,DanaInfo=uphsxnet.uphs.upenn.edu+Results.aspx"
    uri = URI::encode(url)
  end
  
  def last_name_key
    'ctl00$CPHBody$TxtLastName' 
  end
  def first_name_key
    'ctl00$CPHBody$TxtFirstName'
  end
  def phone_number_key
    'ctl00$CPHBody$TxtNumber'
  end
  
  def post_params
    params = {
      '__EVENTTARGET' => '', 
      '__EVENTARGUMENT' => '', 
      '__LASTFOCUS' => '', 
      '__VIEWSTATE' => "/wEPDwUJODE4NDE2OTMxD2QWAmYPZBYKAgEPFCsAAmQQFgJmAgEWAhYCHgtOYXZpZ2F0ZVVybAUdaHR0cDovL3VwaHNuZXQudXBocy51cGVubi5lZHUWAh8ABTJodHRwOi8vdXBoc3huZXQudXBocy51cGVubi5lZHUvcGIvbWFpbi9TZWFyY2guYXNweBYCAgECAWQCAg8PFgIfAAUyaHR0cDovL3VwaHN4bmV0LnVwaHMudXBlbm4uZWR1L3BiL21haW4vU2VhcmNoLmFzcHhkZAIDDw8WAh8ABTRodHRwczovL3d3dy5wZW5ubWVkaWNpbmUub3JnL3BiL21haW4vQWRkUHJvZmlsZS5hc3B4ZGQCBA8PFgIfAAVHaHR0cDovL3VwaHN4bmV0LnVwaHMudXBlbm4uZWR1L3dhZ2Zvcm0vTWFpblBhZ2UuYXNweD9jb25maWc9UGhvbmVCS0RlcHRkZAIFD2QWAgIBD2QWAgIBD2QWAmYPZBYCAhEPZBYEAgUPEGRkFgBkAgkPEGRkFgFmZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUWY3RsMDAkQ1BIQm9keSRDaGtTb3VuZCO/QfmaJs0Nh7aMMG5a5fouPscW", 
      "__PREVIOUSPAGE" =>"LGvxaPcY7PZkrvDYFcvX-pr62ylAE4Y2Ygd52Jnh4xSGP8-Lpp6hYdYZYUwcmra0pcKk8cdIkCNrfp6LC-oGF67F8BeWSmaOW7d9mS_LzPUWNWRt0", 
      '__EVENTVALIDATION' => "/wEWDQL9sunMAQL1+8m8BALz3azcAgKm1KVQAuzb4JIJArCQl/MLAq25g7YOApncg6QMAoT1l+EJAtLMyJIJApDxvY0FAs+B3aUKApji1LEP2V0HX+LWQa+Ohm8tjlJWtmwvVs0=", 
      first_name_key => '',
      last_name_key => '',
      'ctl00$CPHBody$LbEntity' => 'ALL', 
      'ctl00$CPHBody$DdlDiv' => "ALL", 
      phone_number_key => '', 
      'ctl00$CPHBody$HidSession' => '', 
      'ctl00$CPHBody$BtnSubmit' => "Search"}
  end
  
  
  
  def onVPN?() #true if on VPN, false if not, I appologize for using exceptions for flow control
    begin
      Mechanize.new.get('http://uphsxnet.uphs.upenn.edu')
      return true
    rescue Mechanize::ResponseCodeError
      return false
    end
  end
  
end