class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :key
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
