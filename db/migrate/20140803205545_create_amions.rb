class CreateAmions < ActiveRecord::Migration
  def change
    create_table :amions do |t|
      t.string :account
      t.text :users
      t.text :rows
      t.text :on_call
      t.text :all_shifts

      t.timestamps
    end
  end
end
